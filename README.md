<div align="center"><h1><ins>DevOps Demo Project:</ins></h1></div>
<div align="center"><h2>Website Monitoring and Recovery</h2></div>

<div align="center">![1](img/devOps.png)</div>
<br>

<ins>DevOps Tools used in this project</ins>:<br>- Akamai Linode<br>- Docker<br>- Git<br>- GitLab<br>- JetBrains PyCharm<br>- Python<br>- Windows PowerShell

<ins>Web server</ins>:<br>- nginx

<ins>Email server</ins>:<br>- Gmail

<ins>Operating systems</ins>:<br>- Linux<br>- Windows 11

<ins>Project Purpose</ins>: The purpose of this project is to write a scheduled Python program to automatically monitor the status of a nginx server. If the server receives a HTTP response code other than 200, the Python program will send an email notification to IT staff and restart the container. If the server is not accessible to users at all, an email notification to IT staff wil be sent and both the server and the container will be restarted.

<ins>Out of Scope</ins>:<br>- JetBrains PyCharm installation<br>- Linode account creation<br>- Git installation on Windows 11<br>- GitLab account creation

# Project Walkthrough
I. [Preliminary Project Tasks](#prelim)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Create Linode Server](#linode)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Install Docker](#docker)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C) [Run nginx Container](#runnginx)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D) [Python Project Creation](#createproject)<br>
II. [Python Program: monitor-website.py](#python)<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Python File Creation](#file)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Website Request](#website)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C) [Email Notification](#email)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D) [Exception Handling](#exception)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E) [send_notification Function](#send_notification_function)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F) [Restart the nginx Container](#restartnginx)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G) [Restart the Server](#restartserver)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H) [restart_container Function](#restart_container_function)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I) [time Library](#time)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;J) [restart_server_and_container Function](#function)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;K) [Schedule Monitoring Task](#schedule)<br>


## I. Preliminary Project Tasks <a name="prelim"></a>
Prior to writing the Python program, several prerequisites must be met. First, a Linux server hosted by Linode will be created. Second, Docker will be installed on the Linux host server. Next, a nginx container will be created. Finally, the Python project will be created in the JetBrains PyCharm editor.

## A) Create Linode Server <a name="linode"></a>
This section outlines creating a Linux server in the Akamai Linode cloud platform. This section concludes with establishing an SSH session into the server.
1.	Login to the Akamai Linode web portal. On the Linodes page, click the <b>Create Linode</b> button. 

    ![1pl](img/prelim/linode/1.png) 

2.	On the Linodes/Create screen, ensure the Distributions tab is selected. From the Choose a Distribution section, accept the default image as Debian 11 from the dropdown. In the Region section, select the datacenter location closest to the end users (Newark, NJ is selected for this project).  In the Linode Plan section, click on the Shared CPU tab and select the Linode 2GB option. 

    ![2pl](img/prelim/linode/2.png)  

3.	Scroll to the bottom of the screen and enter the root password.

    ![3pl](img/prelim/linode/3.png) 

4.	In the SSH Keys section, click the <b>Add an SSH Key</b> button. 

    ![4pl](img/prelim/linode/4.png)  

5.	In the Add SSH Key pane, specify the label as “python-monitoring.” Minimize the web browser. Open PowerShell and type the following command to display the public SSH Key as output. Copy the key to the clipboard. 

        type ~/.ssh/id_rsa.pub

    ![5pl](img/prelim/linode/5.png) 
 

    Close PowerShell and restore the Akamai Linode web portal. Paste the contents of the clipboard into the SSH Key textbox. Click the <b>Add Key</b> button.

    ![6pl](img/prelim/linode/6.png) 

6.	Return to the SSH Keys section and select the checkbox for the python-monitoring SSH key.
 
    ![7pl](img/prelim/linode/7.png) 

7.	Scroll to the bottom of the page and click the <b>Create Linode</b> button.

    ![8pl](img/prelim/linode/8.png)  

8.	The server will now be in the provisioning state.

    ![9pl](img/prelim/linode/9.png)  

9.	Refresh the screen, and once the server is in the running state, copy the SSH Access command to the clipboard.
 
    ![10pl](img/prelim/linode/10.png) 

10.	Restore PowerShell and paste the SSH command from the clipboard. When prompted to continue connecting, type yes and press Enter.

    ![11pl](img/prelim/linode/11.png)  

    The SSH session into the Linode server is established.

## B)	Install Docker <a name="docker"></a>
This section installs Docker on the Linode-hosted Linux server.

1. Inside the SSH session, confirm the Linux distribution is Debian by entering the following command:

        cat /etc/os-release

    Confirm the value for NAME is listed as Debian.

     ![1pd](img/prelim/docker/1.png) 

2. Navigate to the official Docker documentation for installing the Docker Engine on Debian Linux (https://docs.docker.com/engine/install/debian/).  This project walkthrough will use the apt installation method.

3.	Restore the SSH session and enter the following command to update the Docker’s Apt repository. As the root user is logged in, sudo commands are omitted.

        apt-get update

4.	Run the following command to install ca-certificates, the curl command line tool, and gnupg (GNU Privacy Guard).

        apt-get install ca-certificates curl gnupg

5.	Install to the /etc/apt/keyrings directory (-d) with permissions (-m) set as 0755. The 0755 permission sets full control to the owner, as well as read and execute permissions to the group and everyone else.

        install -m 0755 -d /etc/apt/keyrings

6.	The curl command downloads the official Docker GPG key from the official Docker site. The --dearmor option for the gpg command line tool unshields PEM armors and writes (-o) to the /etc/apt/keyrings/docker.gpg directory. (https://www.gnupg.org/documentation/manuals/gnupg/Operational-GPG-Commands.html). 

        curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg

7.	Grant all users read permission to /etc/apt/keyrings/docker.gpg

        chmod a+r /etc/apt/keyrings/docker.gpg

8.	Add the “stable” Docker repository to Apt and update apt-get to prepare Docker for installation:

        echo \
          "deb [arch="$(dpkg --print-architecture)" signed by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
          "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
          tee /etc/apt/sources.list.d/docker.list > /dev/null
        apt-get update

9.	Install the Docker engine and all relevant plugins (e.g. docker-compose).

        apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

10.	Confirm docker is now available to use by issuing the following command:

        docker -v

    ![2pd](img/prelim/docker/2.png) 

Docker is now installed on the Linux server in Linode.

## C) Run nginx Container <a name="runnginx"></a>
This section uses Docker to create a nginx container on the Linode-hosted Linux server.
1.	In the SSH session, execute the following command to run a nginx container. The -d option runs the container in detached mode, and the -p option specifies the port (8080 being the container port and 80 being the host port):

        docker run -d -p 8080:80 nginx

2.	Run the <b>docker ps</b> command and ensure the container is running:

    ![1pn](img/prelim/nginx/1.png)  

3.	Navigate back to the Akamai Linode web portal. Copy and paste the public IP address to the clipboard.

    ![2pn](img/prelim/nginx/2.png)   

4.	In a separate browser tab, paste the IP address into the address bar and add :8080 as the port. Press Enter and confirm the nginx splash screen is operational:
 
    ![3pn](img/prelim/nginx/3.png)  

5.	Back on the Akamai Linode web portal tab, click inside the new Linux server instance to get more detailed information. Click on the Network Tab and copy the value for Reverse DNS to the clipboard.

    ![4pn](img/prelim/nginx/4.png)   

6.	Open a new browser tab and paste the DNS name into the address bar, adding :8080 as the port.
 
    ![5pn](img/prelim/nginx/5.png)  

The containerized nginx web application prerequisite is complete.

## D) Python Project Creation <a name="createproject"></a>
This schedule outlines the process of creating the Python project in JetBrains PyCharm and pushing the project to the GitLab repository. 
1.	Open the JetBrains PyCharm code editor. In the upper lefthand corner, click the Hamburger Menu (≡) > File > New Project.

    ![1pp](img/prelim/project/1.png)   

2.	On the Create Project screen, specify the location as the root of C:\ and call the project “python-monitor-website-restart-appserver.” For the “New environment using” radio button, leave the default option of Virtualenv from the dropdown. Under this radio button, the second Location box will automatically populate. Click the <b>Create</b> button.
 
    ![2pp](img/prelim/project/2.png) 

3.	On the Open Project dialog box, click the <b>This Window</b> button.

    ![3pp](img/prelim/project/3.png)  

4.	After the virtual environment is created, the project will open with the folder hierarchy in the left pane and the active files in the right pane.

    ![4pp](img/prelim/project/4.png)  

5.	Add a blank README.md file by right-clicking the root of the project folder > New > File. Name the file README.md.

    ![5pp](img/prelim/project/5.png)  

    ![6pp](img/prelim/project/6.png) 

6.	In a browser, login to GitLab. On the Projects screen, click the <b>New project</b> button in the right corner.

    ![7pp](img/prelim/project/7.png) 

7.	On the Create new project screen, click <b>Create blank project</b>.

    ![8pp](img/prelim/project/8.png)  

8.	On the Create blank project screen, name the project “python-monitoring-website-restart-appserver.” In the Project URL section, specify the username from the dropdown (e.g. jlomba09), and ensure the project slug matches the project name. Set the visibility level to Public and uncheck the box to initialize the repository with a README. Click the <b>Create project</b> button.

    ![9pp](img/prelim/project/9.png)  

9. In the bottom lefthand corner of PyCharm, click the Terminal icon to open a PowerShell prompt. 

    ![10pp](img/prelim/project/10.png) 

10. <ins>Note</ins>: Although the installation of Git on Windows 11 is outside the scope of this project walkthrough, the Git installer can be found here: https://git-scm.com/download/win.

    Initalize the Git repository.

        git init

    The output will show the repository is initialized.

    ![11pp](img/prelim/project/11.png) 

11. Set the origin as the newly created “python-monitor-website-restart-appserver” project in GitLab:

        git remote add origin git@gitlab.com:jlomba09/python-monitor-website-restart-appserver.git

12. Add changes and set an initial commit message:

        git add .
        git commit -m “Initial commit”

13. Push the GitLab repository to the master branch with the following command:

        git push -u origin master

14. Refresh the Python project in GitLab and confirm the README file exists:
 
    ![12pp](img/prelim/project/12.png) 

The Python code to be written in this project walkthrough can now be pushed to GitLab.

## II. Python Program: monitor-website.py <a name="python"></a>
This section provides a comprehensive walkthrough of writing the monitor-website.py Python program. First, the Python file is created in JetBrains PyCharm. Second, logic is written for a website request where an <b>if/else</b> statement is evaluated based on whether the HTTP status code is 200. Next, an email notification is sent to IT staff in the event the HTTP status code is a value other than 200. Then, exception handling is integrated into the Python program. This section continues with writing logic for restarting the nginx container and server. For code that repeats in the program, the logic for sending email, restarting the container, and restarting the server is converted into functions. A time library is incorporated into the project for adding additional time between the server's running state and starting the nginx container. Finally, the program uses the schedule library to automatically run on a scheduled cadence. 
## A) Python File Creation <a name="file"></a>
First, the Python file will be created in JetBrains PyCharm.
1. Restore the PyCharm editor and create a new file by right-clicking the root of the project folder > New File. 

    ![1ppf](img/python/file/1.png)  

2. Name the file monitor-website.py and press the Enter key.

    ![2ppf](img/python/file/2.png) 

3. When prompted to add the file to Git, click the <b>Add</b> button.

    ![3ppf](img/python/file/3.png)
 
4. The new monitor-website.py Python file will now appear in the right pane.
 
    ![4ppf](img/python/file/4.png)

## B) Website Request <a name="website"></a>
The purpose of the following Python logic is to programmatically access the nginx website from Python by DNS name. This mirrors a user browsing out to the nginx website in a web browser.
1. In the bottom left corner of the PyCharm editor, click the Terminal icon to open a PowerShell pane at the bottom of the screen. 

    ![1ppwr](img/python/website_request/1.png) 

2. In the PowerShell window, use the <b>pip</b> command to install the requests library from PyPi.

        pip install requests

3. Confirm the requests library is installed by expanding the External Libraries folder > < Python 3.11 > site-packages. The requests library will now be present.

    ![2ppwr](img/python/website_request/2.png) 

4. With the requests library installed, begin the monitor-website.py file by importing the requests library:

        import requests

    The remainder of code in this section pertaining to the requests library references the following documentation (https://pypi.org/project/requests/). 

5. Restore the Linode web portal and copy the DNS name of the Linux server to the clipboard. This is shown in Section I.C "Run nginx Container," Step 5 above.

6. Use the get function of the requests module. Paste the URL from Step 5 as a parameter to the get function, enclosed in parentheses and single quotes.

        requests.get('http://66-228-39-99.ip.linodeusercontent.com:8080/')

7. Set the get function of the requests module as a variable called response.

        response = requests.get('http://66-228-39-99.ip.linodeusercontent.com:8080/')

8. Use the built-in <b>print</b> function to print the response from the nginx application.

        print(response)

9. Run the program by right-clicking the monitor-website-py tab > Run ‘monitor-website.’
 
    ![3ppwr](img/python/website_request/3.png) 

10. A Run pane will display at the bottom of PyCharm. Confirm the response is OK, as denoted by the 200 HTTP response code. For more information about HTTP response codes, reference the following link: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status 

    ![4ppwr](img/python/website_request/4.png)  

11. To see the HTML displayed in the Run window, in the <b>print</b> statement, call the text function on the response variable:

        print(response.text)

    Rerun the Python program. The output is displayed below:
 
    ![5ppwr](img/python/website_request/5.png) 

12. To display the numeric value of the HTTP response code 200, in the <b>print</b> statement, replace calling the text function with calling the status_code function:

        print(response.status_code)

    Rerun the Python program. The output is displayed below showing the 200 HTTP response code:
 
    ![6ppwr](img/python/website_request/6.png) 

13. Printing the output of response.status_code from Step 12 provides the foundation to formulate a conditional <b>if/else</b> statement for printing different output based on the HTTP status code.

    First, delete the following <b>print</b> statement:

        print(response.status_code)

    If the HTTP status code is 200, use an <b>if</b> statement to display output on the screen that the application is running successfully:

        if response.status_code == 200:
            print(‘Application is running successfully!’)

    If the HTTP status code is a value other than 200, use an <b>else</b> statement to print that the application is down.

        else:
            print(‘Application Down. Fix it!’)

14.	The monitor-website.py program up to this point is shown below:

        import requests
    
        response = requests.get('http://66-228-39-99.ip.linodeusercontent.com:8080/')

        if response.status_code == 200:
            print('Application is running successfully!')
        else:
            print('Application Down. Fix it!')

## C) Email Notification <a name="email"></a>
If the HTTP status code renders a value other than 200, there is a problem with the nginx web application and user access is unavailable. In this section, the Python program will introduce logic to automatically send an email notification whenever the web application is down.


1.	Begin by importing the built-in smtplib library. Add the <b>import</b> statement underneath the existing import requests statement. 

        import smtplib

    <ins>Note</ins>: Refer to the official Python documentation for more information about the smtplib library (https://docs.python.org/3/library/smtplib.html).

2.	Next, the SMTP server and port will be specified in the Python program.

    In this project scenario, Gmail is used. The below screenshot is taken from official Google documentation and shows the SMTP server address and associated port (https://support.google.com/a/answer/176600?hl=en): 

    ![0ppen](img/python/email_notification/0.png)  

    Using this information, locate the <b>else</b> statement written in monitor-website.py. Under the <b>print</b> statement, enter a couple carriage returns. Then, use the SMTP function to specify the use of Gmail's SMTP server on port 587: 

        smtplib.SMTP('smtp.gmail.com', 587)

3. As Gmail is an external application, a <b>with</b> statement will be needed for exception handling. Exceptions for Gmail may include login or connection-related errors, both of which are outside the control of the Python application.

    Use the following syntax to incorporate smtplib.SMTP('smtp.gmail.com', 587) from Step 2 as part of the <b>with</b> statement. The <b>as</b> keyword stores the smtplib.SMTP function as a variable called smtp:

        with smtplib.SMTP('smtp.gmail.com', 587) as smtp:

4.	Inside the <b>with</b> statement block, call the starttls function on the smtp variable to ensure the connection between Gmail and the monitor-website.py Python program is encrypted.

        smtp.starttls()

5.	Inside the <b>with</b> statement block and under smtp.starttls(), enter a couple carriage returns. Call the ehlo() function on the smtp variable in order for Gmail to recognize the monitor-website.py Python program.

        smtp.ehlo()


6.	For enhanced security, this program assumes that 2-Step Verification is turned on in the Google account. Before the smtp.login statement can be written in the <b>with</b> block, an application password will be generated in the Google account. The application password provides permission to the Python program to act on the sender’s behalf.

    Open a web browser and navigate to https://myaccount.google.com. In the search bar, type in app and select App passwords in the search results.
 
    ![1ppen](img/python/email_notification/1.png)  

7. On the App passwords screen, enter website-monitor as the app name and click the <b>Create</b> button.

    ![2ppen](img/python/email_notification/2.png)  

8. On the Generated app password dialog box, the app password will display on the screen. Copy the password to the clipboard and temporarily paste it in a text editor of choice. Click the <b>Done</b> button.

    ![3ppen](img/python/email_notification/3.png)  

9.	Confirm website-monitor now appears in the list of app passwords. Minimize the Google account management webpage.

    ![4ppen](img/python/email_notification/4.png)   

10.	Return to the monitor-website.py Python program in the PyCharm editor. At the top of the program under the other <b>import</b> statements, import the built-in os library. More information about the os library can be found here (https://docs.python.org/3/library/os.html). 

        import os

11.	With security in mind, the os library will be used to store the email username and password as environment variables so the credentials are not hardcoded in the program. Call the environ function of the os module and get the value of the environment variables. Set both as variables natively in Python:

        EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
        EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')

    <ins>Note</ins>: EMAIL_ADDRESS and EMAIL_PASSWORD are known as constants, as they are set once and do not change. Constants should be in all caps to distinguish them from other native Python variables.

12.	In the <b>with</b> statement, under smtp.ehlo(), call the login function on the smtp variable and pass EMAIL_ADDRESS, and EMAIL_PASSWORD as parameters.

        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)

13.	In the upper righthand corner of PyCharm, click Current > Edit Configurations…

    ![5ppen](img/python/email_notification/5.png)  

14.	On the Run/Debug Configurations screen, click the <b>+</b> button.

    ![6ppen](img/python/email_notification/6.png)   

15.	In the Add New Configuration pane, select Python.

    ![7ppen](img/python/email_notification/7.png)   

16.	Name the configuration the same as the Python program (monitor-website). In the Run section, leave the script dropdown selected. Browse out to the absolute path of the monitor-website.py program (i.e. C:\python-monitor-website-restart-appserver\monitor-website.py). Set the working directory to the root path of the project folder (C:\python-monitor-website-restart-appserver). 

    ![8ppen](img/python/email_notification/8.png)   

17.	On the same screen as Step 16, click the icon in the Environment variables textbox.

    ![9ppen](img/python/email_notification/9.png)   

18.	On the Environment Variables dialog box, click the + sign and add EMAIL_ADDRESS and EMAIL_PASSWORD as environment variables. From Step 8, paste the application password from the text editor as the value for EMAIL_PASSWORD. Click <b>OK</b>.

    ![10ppen](img/python/email_notification/10.png)   

19.	Back on the Run/Debug Configurations screen, click <b>OK</b> to return to the editor.

    ![11ppen](img/python/email_notification/11.png)   

20.	Before the last function can be called to send an email, set a variable for the message. The \n in Python starts a new line, but in this case, also functions to separate the subject from the message body.

        msg = "Subject: SITE DOWN\nFix the issue! Restart the application."

21.	Last, call the sendmail function on the smtp variable and pass 3 parameters: the sender’s email address, the recipient's email address, and the message. For this project's use case, assume the sender and receiver are the same.

        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, msg)

22.	In the right hand corner of PyCharm, click the monitor-website dropdown. Next to the application name, click the green triangle icon to run the application.

    ![12ppen](img/python/email_notification/12.png)   

23.	As the nginx container is currently in the running state, the monitor-website application executes the <b>if</b> logic of the program which prints output that the application is running successfully.

    ![13ppen](img/python/email_notification/13.png)   

24.	To simulate the application being down, comment out the existing <b>if</b> statement. Add another <b>if</b> statement, setting it to False, to trigger the <b>else</b> statement to execute. Rerun the program.

        # if response.status_code == 200:
        if False:
            print('Application is running successfully!')

    The output from the Run pane confirms the <b>else</b> statement is executed.

    ![14ppen](img/python/email_notification/14.png)   

25.	Check Gmail and confirm the email is received. Notice the email message mirrors exactly what is set as the msg variable in Step 20.

    ![15ppen](img/python/email_notification/15.png)  
 
26. With testing complete, return to the PyCharm code editor and delete the if False: line of code from Step 24. Uncomment the original <b>if</b> statement.

## D) Exception Handling <a name="exception"></a>

<ins>Issue</ins>: The Python program thus far does not account for instances when no response is returned. In these cases, the following line of code (the response) throws an exception:

    response = requests.get('http://66-228-39-99.ip.linodeusercontent.com:8080/')

An exception can occur if the connection is refused, there is a request timeout, or the nginx container is stopped. If the program is run in these cases, the <b>if/else</b> logic will not be executed at all. This issue will be simulated in the following steps:

1.	SSH into the Linux host where the nginx container resides. 

2.	Execute the following command to identify the container ID of the nginx container:

        docker ps

3.	Copy the container ID to the clipboard and execute the following command to stop the nginx container:

        docker stop <container-id>

     ![1ppeh](img/python/exception/1.png)  

4. With the nginx container stopped, return to PyCharm and rerun the program. Sample output from the exception is shown below:
 
     ![2ppeh](img/python/exception/2.png)

<ins>Resolution</ins>: For handling exceptions, a <b>try/except</b> block will be incorporated into the Python program. First, the code will be tested to ensure the <b>except</b> block works as expected. Then, an email notification will be sent in the event an exception occurs.

1.	Begin by adding a <b>try</b> block underneath the response variable. Indent the entire <b>if/else</b> logic inside the <b>try</b> block as shown below. Move the response variable inside the <b>try</b> block and above the <b>if/else</b> statement. The <b>try</b> block will attempt to execute the <b>if/else</b> statement if a response is returned:

        try:
            response = requests.get('http://66-228-39-99.ip.linodeusercontent.com:8080/')
            if response.status_code == 200:
                print('Application is running successfully!')
            else:
                print('Application Down. Fix it!')
                # send email to me
                with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
                    smtp.starttls()
                    smtp.ehlo()
                    smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
                    msg = "Subject: SITE DOWN\nFix the issue! Restart the application."
                    smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, msg)

2.	At the same indentation level as the <b>try</b> block, create an <b>except</b> block. The <b>except</b> block will execute if no response from the requests.get function is returned. As shown in the graphic below, hover the mouse cursor over the word Exception and notice this is a base class for all non-exit exceptions. Much like smtp is a variable in the <b>with</b> statement, <b>ex</b> functions the same as a variable in the <b>except</b> statement:
  
     ![3ppeh](img/python/exception/3.png)

3.	Inside the <b>except</b> block, write a <b>print</b> statement that displays a connection error occurred and print out the <b>ex</b> variable that shows the exception. The f' denotes the variable will be included inline in the string:

        print(f'Connection error happened: {ex}')

4.	Rerun the program and notice from the output the <b>except</b> block is executed. The HTTPCoonectionPool exception message is displayed below:
 
     ![4ppeh](img/python/exception/4.png)

With the <b>except</b> block confirmed to be functional, the next section covers additional logic in the <b>except</b> block to send an email alert to IT staff.

## E) send_notification Function <a name="send_notification_function"></a>
Recall the existing <b>else</b> statement includes a <b>with</b> block with logic to send email. The same logic for sending email needs to be used in the <b>except</b> block to notify IT staff whenever an exception occurs. First, this section demonstrates code duplication when sending an email in both the <b>else</b> and <b>except</b> blocks. Then, a function is implemented to eliminate the need to write duplicate logic.

1.	First, copy the <b>with</b> block from the <b>else</b> statement to the clipboard. Paste the <b>with</b> block into the <b>except</b> block as shown below:

        except Exception as ex:
            print(f'Connection error happened: {ex}')
            with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
                smtp.starttls()
                smtp.ehlo()
                smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
                msg = "Subject: SITE DOWN\nFix the issue! Restart the application."
                smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, msg)

2.	In the <b>else</b> statement, modify the message in the <b>with</b> block as follows to include the response.status_code variable. Doing so will include the HTTP status code in the email. Once again, the f" formats the string so that the variable can be included inline in a clean manner:

        msg = f"Subject: SITE DOWN\nApplication returned {response.status_code}. Fix the issue! Restart the application."

3. In the <b>except</b> statement, modify the value of the msg variable in the <b>with</b> block to indicate the application is not accessible at all. This also indicates that no HTTP response code is returned.

        msg = "Subject: SITE DOWN\nApplication not accessible at all."


Now that the duplicate code has been demonstrated, a function will be introduced to remove the code duplication.

1. First, under the EMAIL_ADDRESS and EMAIL_PASSWORD constants, define a function called send_notification as follows:

        def send_notification():

2. Inside the send_notification function, write a <b>print</b> statement to display output to the user that an email is being sent.

        print('Sending an email...') 

3. Copy one of the <b>with</b> blocks from the <b>except</b> or <b>else</b> blocks and paste it underneath the <b>print</b> statement as shown below:

        def send_notification():
            print('Sending an email...') 
            with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
                    smtp.starttls()
                    smtp.ehlo()
                    smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
                    msg = f"Subject: SITE DOWN\nApplication returned {response.status_code}. Fix the issue! Restart the application."
                    smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, msg)

4.	In the <b>else</b> statement of the <b>try</b> block, delete the first <b>with</b> block and replace it by calling the send_notification function. Delete the “send email to me” comment.

        else:
            print('Application Down. Fix it!')
            send_notification()

5.	As the message body is the only thing that changes in the <b>with</b> blocks, the message body will be passed as a parameter in the send_notification function. Assume the subject will stay the same, but the message body will change based on whether the <b>else</b> or <b>except</b> statement is executed. Replace the message body with the newly passed email_msg parameter:

        def send_notification(email_msg):
        ...
        msg = f"Subject: SITE DOWN\n{email_msg}"

6.	In the <b>else</b> statement of the <b>try</b> block, reinsert the message variable by setting it to the message body (e.g. Application returned HTTP status code). Place the message variable between the <b>print</b> statement and send_notification() function call. 

        msg = f'Application returned {response.status_code}'

    Pass the message variable as a parameter to the function call. 

        send_notification(msg) 

7.	In the <b>except</b> block, modify the message variable so that it is only set to the message body, as done in Step 6. The subject should not be included as it is already accounted for in the function.

        msg = f'Application is not accessible at all.'

    Delete the entire <b>with</b> block and call the send_notification again, passing the message variable as a parameter.

        send_notification(msg)
 
8.	The following shadows name warning will appear in the PyCharm editor:

     ![1ppsn](img/python/send_notification/1.png) 

    This error is caused by the message variable being the same in the function (globally) as it is in the context of the <b>if/else</b> statement. To remedy this issue, in the send_notification function, rename the msg variable to message as shown below:. 

        message = f"Subject: SITE DOWN\n{email_msg}"

    Also, in the sendmail function call on the smtp module, rename the msg variable that is passed as a parameter:

        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, message)

9.	With the nginx docker container still in a stopped state, rerun the Python program. The following output shows the <b>except</b> statement is executed.
 
     ![2ppsn](img/python/send_notification/2.png)

10.	Check Gmail and notice the message body is the same as the msg variable in the <b>except</b> statement.

     ![3ppsn](img/python/send_notification/3.png) 

With IT staff now receiving email notifications that there is an issue with the application, it is time to take action to remedy the issue.
## F) Restart the nginx Container <a name="restartnginx"></a>
In the event the application code returns a value other than 200, one potential fix is to restart the nginx container. The following logic will be written in the <b>else</b> block and will leverage the Paramiko library. The Paramiko library will allow the program to SSH into the Linode server and run the <b>docker start</b> command.
1.	In the lefthand corner of PyCharm, click the Terminal icon to open PowerShell in the bottom pane. 

     ![1pprn](img/python/restart_nginx/1.png)  

2. In the PowerShell window of the PyCharm editor, use <b>pip</b> to install the Paramiko library. This library will provide the ability to SSH into the Linode server and run Linux commands. The remainder of this section references the official Paramiko documentation located here: https://docs.paramiko.org/en/3.3/api/client.html 

        pip install paramiko

3.	Confirm the Paramiko library is successfully installed by observing the terminal output:
 
     ![2pprn](img/python/restart_nginx/2.png)  

4.	At the top of the monitor-website.py file, <b>import</b> the Paramiko library. 

        import paramiko

5.	Right after the send_notification function call in the <b>else</b> statement, enter a couple carriage returns. Write a comment to indicate the following logic will restart the application.

        # restart the application

6.	Under the comment, call the SSHClient function on the Paramiko module to initialize a SSH client in the Python program. Set the function call to a variable named ssh.

        ssh = paramiko.SSHClient()

7.	Under the SSHClient function call, call the set_missing_host_key_policy function on the ssh variable. Pass the AutoAddPolicy function as a parameter within the function to dynamically add the local workstations’ missing host key to the Linode server. This will eliminate the need to interactively type Y when accessing the server by SSH for the first time.

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())               

8.	Under the set_missing_host_key_policy function call, write a line of code to call the connect function on the ssh variable to establish a SSH connection. Specify three named parameters: hostname, username, and key_filename. The hostname is the IP address of the Linode server and the username is root. The key_filename is the absolute path of the workstation’s private SSH key.  

        ssh.connect(hostname='66.228.39.99', username='root', key_filename='C:/Users/Joseph/.ssh/id_rsa')

    <ins>Note</ins>: the named port parameter is intentionally omitted as it is understood to use the default (port 22).

    <ins>Note</ins>: When the ssh command is issued in a terminal, the -i option is not required if the default location of the private SSH key (~/.ssh/id_rsa) is used. Regardless, the Python program <ins>requires</ins> the absolute path to the private SSH key no matter where the private SSH key resides.

9.	Under the connect function fall, write a line of code to call the exec_command function on the ssh variable to issue Linux commands. For testing purposes, the Linux command that will be issued is the <b>docker ps</b> command. Set the docker execution command to standard input, standard output, and standard error variables.

        stdin, stdout, stderr = ssh.exec_command('docker ps')

    <ins>Note</ins>: Standard input is what the engineer types on their keyboard inside the terminal. Standard output is the results that display on the screen. Standard error is output that is shown if an error occurs.
10.	For testing, add an if False: line of code to bypass the <b>if</b> statement and run the <b>else</b> logic. For now, comment out the initial <b>if</b> statement.

        if False:
        # if response.status_code == 200:

11.	Under the stdin, stdout, and stderr variables, <b>print</b> out the standard output. 

        print(stdout)

12.	Rerun the program and notice from the output that Paramiko generates a file.

     ![3pprn](img/python/restart_nginx/3.png)   

13.	To read what is inside the ChannelFile, modify the <b>print</b> statement from Step 11 by calling the readlines function on the stdout variable.

        print(stdout.readlines())

14.	Rerun the program and notice the <b>docker ps</b> command displays output as expected.

     ![4pprn](img/python/restart_nginx/4.png)   

15.	On the terminal window, scroll over and notice the container ID is displayed. Copy the container ID to the clipboard. Modify the docker command from step 9 to start the container

        stdin, stdout, stderr = ssh.exec_command('docker start 4d189c7fcd4b')

16.	Under the <b>print</b> statement that displays stdout, close the SSH session by calling the close function on the ssh variable.

        ssh.close()

17.	After closing the SSH session, add a final line to display output to the user that the application has restarted.

        print('Application restarted')


18.	Delete the if False: statement and uncomment the original <b>if</b> statement.

## G) Restart the Server <a name="restartserver"></a>
Recall the <b>except</b> block handles the scenario where no response is returned because the server is not accessible to users. In this scenario, restarting the server itself will act to remedy the issue. This section introduces the linode_api4 library to interface with Linode resources.

1.	In the lefthand corner of PyCharm, click the Terminal icon to open PowerShell in the bottom pane. 

     ![1pprs](img/python/restart_server/1.png)   

2.	In the PowerShell window of the PyCharm editor, use <b>pip</b> to install the Linode library. This library will provide the ability to interact with Linode resources (e.g. reboot a Linode server). The remainder of this section references the official Linode documentation located here: https://pypi.org/project/linode-api4/ 

        pip install linode-api4

3.	Confirm the Linode library is successfully installed by observing the terminal output:
 
     ![2pprs](img/python/restart_server/2.png)  

4.	At the top of the monitor-website.py file, <b>import</b> the Linode library. 

        import linode_api4

5.	In the <b>except</b> block after the send_notification function call, enter a couple carriage returns and write a comment to denote that logic will be written to restart the Linode server:

        # restart Linode server

6. Under the restart Linode server comment, write a <b>print</b> statement to display output to the user the server is rebooting.

        print('Rebooting the server...')

7.  Under the restart Linode server comment, call theLinodeClient function on the linode_api4 module to establish a connection to the Linode account. Set the function call to a variable named client.

        client = linode_api4.LinodeClient()

8. If the mouse hovers over the LinodeClient function, a warning appears that a token is missing. The token is required for authentication to the Linode account.

     ![3pprs](img/python/restart_server/3.png)  
 
9. Minimize the PyCharm editor and login to the Linode web portal. In the righthand corner, click the username of the account. In the My Profile section, click API tokens.

     ![4pprs](img/python/restart_server/4.png)   

10.	Click the <b>Create a Personal Access Token</b> button.

     ![5pprs](img/python/restart_server/5.png)   


11. In the Add Personal Access Token pane, label the token as “python.” In the Select All row, select the Read/Write radio button. Click the <b>Create Token</b> button.
 
     ![6pprs](img/python/restart_server/6.png)  

12. This is the one and only time the API token will be viewable. Copy the Personal Access Token to the clipboard and temporarily store it in a text editor of choice. Click the <b>I Have Saved My Personal Access Token</b> button.

     ![7pprs](img/python/restart_server/7.png)  

13. Confirm the python token now appears in the Personal Access Tokens section. Minimize the Linode web portal.

     ![8pprs](img/python/restart_server/8.png)   

14. Restore the PyCharm editor. For security reasons, the os library will again be used to store the API token as an environment variable so as to not be hardcoded into the program. Under the other constants, create a new constant called LINODE_TOKEN. As with EMAIL_ADDRESS and EMAIL_PASSWORD, use the environ function on the os module to get the environment variable. Set the function call as a Python variable named LINODE_TOKEN as shown below:

        LINODE_TOKEN = os.environ.get('LINODE_TOKEN')

15. Return to the <b>except</b> block. Locate the restart Linode server comment. For the client variable, pass LINODE_TOKEN as a parameter to the LinodeClient function call on the linode_api4 module.

        client = linode_api4.LinodeClient(LINODE_TOKEN)

16. At the top right of PyCharm, click the monitor-website dropdown > Edit Configurations...

     ![9pprs](img/python/restart_server/9.png)  
 
17. On the Run/Debug Configurations screen, click the icon next to the Environment variables textbox.
 
     ![10pprs](img/python/restart_server/10.png)  

18. Click the + button and add LINODE_TOKEN as an environment variable, pasting the API token as the value from the text editor. Click the <b>OK</b> button.

     ![11pprs](img/python/restart_server/11.png)   

19. Restore the Linode web portal. Click on Linodes in the left menu. In the right pane, click on the Linode instance for a more detailed view.

     ![12pprs](img/python/restart_server/12.png)  
 
20. Copy the Linode ID number to the clipboard. Minimize the Linode web portal.

     ![13pprs](img/python/restart_server/13.png)   

21. Restore the PyCharm editor. Under the client variable in the <b>except</b> block, call the load function on the client variable. Pass two parameters: the first one to tell the load function the type of resource to connect to (an instance) and the second one for the instance ID. Paste the instance ID from the clipboard as the second parameter. Set the load function call as a variable named nginx_server as shown below:

        nginx_server = client.load(linode_api4.Instance, 50867697)

22. Finally, pass the reboot function to the nginx_server variable to restart the server.

        nginx_server.reboot()

23. Open a PowerShell window and SSH into the Linux host where the nginx container resides. Enter the <b>docker ps</b> command to reveal the container ID. Copy the container ID to the clipboard. Issue the <b>docker stop</b> command to stop the container. 

        docker ps
        docker stop <container-id>

24. Rerun the program. Output will display on the screen that a connection error occurred. 
 
     ![14pprs](img/python/restart_server/14.png)  

    Restore the Linode web portal and notice the server is in a rebooting state. 

     ![15pprs](img/python/restart_server/15.png)  
 
25. In the Linode web portal, confirm the Linux server returns to a running state.

     ![16pprs](img/python/restart_server/16.png)   

## H) restart_container Function <a name="restart_container_function"></a>
After the server is restarted, the nginx container also needs to be restarted to allow the website to be accessible to users again. Recall the logic for restarting the container is already accounted for in section G of this project walkthrough. Since logic will repeat more than once in this program, a restart_container function will be created in this section. After creation, the restart_container function can be called in the two places it appears in the program.
1.	Underneath the logic for restarting the Linode server in the <b>except</b> block, enter a couple carriage returns and write another comment to denote restarting the application.

        # restart the application

2.	Towards the top of the monitor-website.py file, under the send_notification function, enter a couple carriage returns and define a new function called restart_container.

        def restart_container():

3.	In the <b>else</b> statement of the <b>try</b> block, cut the logic from under the restart the application comment and paste it underneath the restart_container function. Move the 'application started' <b>print</b> statement from the bottom to the top of the function as the first line; modify the <b>print</b> statement to state 'Restarting the application...' 

    The below code summarizes these changes:

        def restart_container():
            print('Restarting the application…’)
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname='66.228.39.99', username='root', key_filename='C:/Users/Joseph/.ssh/id_rsa')
            stdin, stdout, stderr = ssh.exec_command('docker start 4d189c7fcd4b')
            print(stdout.readlines())
            ssh.close()

4.	Call the restart_container function under the <b>else</b> statement of the <b>try</b> block:

        # restart the application
        restart_container()

5.	In the <b>except</b> block, it will take some time for the server to be back online before the nginx container can be restarted. Implementing an infinite <b>while</b> loop is a good use case here when dealing with issues pertaining to time.  Under the 'restart the application' comment in the <b>except</b> block, create an infinite <b>while</b> loop:

        # restart the application
        while True:

6. As the first line under the infinite <b>while</b> loop, load the Linode client again as done in section G, Step 21. 

        nginx_server = client.load(linode_api4.Instance, 50867697)

7.	Under the nginx_server variable, write an <b>if</b> statement to continually check the server status. Do this by calling the status function on the nginx_server variable and set the conditional value to 'running':

        if nginx_server.status == 'running':

8.	Under the <b>if</b> statement of the <b>while</b> loop, call the restart_container function.

        restart_container()

9.	Break the infinite loop so it does not run forever.

        break

10.	Rerun the Python program. Refresh the Linode web portal and confirm the server is in the rebooting state.
 
     ![1pprc](img/python/restart_container/1.png)  

    Browse to the IP and port of the nginx container and confirm the application is accessible.

     ![2pprc](img/python/restart_container/2.png)   

11.	Observe the output from the Run window, noticing the various output messages from the <b>print</b> statements.

     ![3pprc](img/python/restart_container/3.png)  

12.	Open PowerShell and SSH into the Linode server where the nginx container resides. Run the <b>docker ps</b> command to confirm the nginx container is running.

     ![4pprc](img/python/restart_container/4.png)  

## I) time Library <a name="time"></a>
To fail-proof the <b>while</b> loop written in the <b>except</b> block, the built-in time library will be leveraged as a precaution to factor in additional time between the server being in the running state and starting the nginx container. More information about the built-in time library can be found here:  https://docs.python.org/3/library/time.html 
1.	At the top of the monitor-website.py file, write an <b>import</b> statement underneath the other import statements to import the built-in time library:

        import time

2. In the <b>if</b> statement of the <b>while</b> loop in the <b>except</b> block, call the sleep function of the time module and specify 5 seconds as the parameter.


        time.sleep(5)

3.	The complete infinite <b>while</b> loop in the <b>except</b> block will now look like this:

        # restart the application
        while True:
            nginx_server = client.load(linode_api4.Instance, 50867697)
            if nginx_server.status == 'running':
                time.sleep(5)
                restart_container()
                break

## J) restart_server_and_container Function <a name="function"></a>

As best practice for tidying up the code into digestible and manageable blocks that can be reused, this section will detail placing the logic for restarting the server and container into its own function.
1.	Scroll to the top of the monitor-website.py program. Under the LINODE_TOKEN constant, enter a couple carriage returns. Define a new function called restart_server_and_container:

        def restart_server_and_container():

2.	Scroll to the bottom of the monitor-website.py program. In the <b>except</b> block, cut the entire code from the two comments (restart Linode server and restart the application) and paste it underneath the restart_server_and_container function. The restart_server_and_container function will now look like this:

        def restart_server_and_container():
            # restart Linode server
            print('Rebooting the server...')
            client = linode_api4.LinodeClient(LINODE_TOKEN)
            nginx_server = client.load(linode_api4.Instance, 50867697)
            nginx_server.reboot()

            # restart the application
            while True:
                nginx_server = client.load(linode_api4.Instance, 50867697)
                if nginx_server.status == 'running':
                    time.sleep(5)
                    restart_container()
                    break

3.	Under the send_notification function call in the <b>except</b> block, call the restart_server_and_container function without parameters. The complete <b>except</b> block will now look like this:

        except Exception as ex:
            print(f'Connection error happened: {ex}')
            msg = f'Application is not accessible at all.'
            send_notification(msg)
            restart_server_and_container()

## K) Schedule Monitoring Task <a name="schedule"></a>
For monitoring purposes, the final section of this project walkthrough outlines using the schedule library to dynamically run the program on a regularly scheduled cadence. For the sake of this project, the application will run every 5 minutes but can be configured to run every hour or other desired interval. 
1.	In the lefthand corner of PyCharm, click the Terminal icon to open PowerShell in the bottom pane. 

     ![1pps](img/python/schedule/1.png)  

2.	In the PowerShell window of the PyCharm editor, use <b>pip</b> to install the schedule library. This library will allow the program to be scheduled at a specified interval. The remainder of this section references the official PyPi documentation located here: https://pypi.org/project/schedule/ 

        pip install schedule

3.	Confirm the schedule library is successfully installed by observing the terminal output:

     ![2pps](img/python/schedule/2.png)   

4.	At the top of the monitor-website.py file, underneath the other import statements, <b>import</b> the schedule library.

        import schedule

5.	Above the <b>try</b> block, create a function called monitor_application.

        def monitor_application():

6.	Select the entire contents of the <b>try</b> and <b>except</b> blocks and indent them inside the monitor_application function.

7.	Beneath the monitor_application function, use the schedule library to run the monitor_application function every 5 minutes. 

        schedule.every(5).minutes.do(monitor_application)

8.	Last, at the very bottom of the Python file, implement the scheduled task in an infinite loop.

        while True:
            schedule.run_pending()

9.	For testing, change the interval from minutes to seconds. 

        schedule.every(5).seconds.do(monitor_application)

10.	Rerun the application and observe output. The nginx container is currently in the running state; thus, the program will continue to print the application is running successfully until the program is stopped.

     ![3pps](img/python/schedule/3.png)  
 
<ins>Project Cleanup:</ins> Remember to destroy the Linode server and delete the API token when complete.

